This directory contains OpenCPI tutorials. A tutorial is an interactive,
“how to” learning document that teaches by example and supplies the 
information necessary to complete a particular task. A tutorial presents
one or more objectives and goals and one or more step-by-step examples
that demonstrate how to achieve these objectives and goals. A tutorial
requires a reader to participate by performing each step that the
tutorial describes.

OpenCPI provides the following tutorials:

Tutorial 1: Introduction to OpenCPI Component-based Application Development.
This tutorial demonstrates the OpenCPI application and component development
workflow and processes using a simple application.

Tutorial 2: Designing Applications and HDL Assemblies. This tutorial
demonstrates how to create an application from components that exist
in other libraries in other projects and how to create different HDL
assemblies for the application.

Tutorial 3: Developing an RCC Worker. This tutorial demonstrates the
design process for an RCC worker implementation.

Tutorial 4: Using Third-party Libraries in RCC Workers. This tutorial
demonstrates how to integrate a third-party library into an RCC worker.

These tutorials are meant to be run in sequence.

