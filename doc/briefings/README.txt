This directory contains OpenCPI briefings. A briefing consists of information
formatted as a set of slides so that it can be presented to others. 
OpenCPI briefings provide background information about OpenCPI concepts
and processes. 

OpenCPI briefings Include:

Briefing 1: Introduction to OpenCPI. This briefing gives a high-level description
of OpenCPI technology, its goals, history, concepts and processes and associated
terminology. It is intended to provide first-level introductory information
to those who are new to OpenCPI.
 
Briefing 2: Introduction to OpenCPI Development. This briefing describes the
OpenCPI technology from the development point of view, including information
about concepts of operation and comparisons with other similar technologies.

Briefing 4: Component and Worker Development Overview. This briefing describes
component and worker authoring models using an example to illustrate the models.
It also provides information about control plane life-cycle and configuration,
component ports and protocol and the application development workflow.

Briefing 5: Automated Test_Suite Introduction. This briefing introduces
the Automated Unit Test Suite for components. It describes how it works
and how to use it and provides detailed information about Automated Test
Description XML File elements and attributes.

Briefing 6; OpenCPI RCC Development. This briefing introduces RCC Development
concepts and elements and provides information about advanced RCC development
features.

Briefing_7: Automated Test_Suite: Details. This briefing provides detailed
information about the Automated Test Suite, including file I/O, buffer size
handling, when to use the HDL simulator and hardware test assemblies, and
how to debug failed tests.

Briefing 8: HDL Application Workers. This briefing describes HDL worker
concepts, elements and functions and provides an example of an HDL
application worker.

Briefing 9: HDL Assemblies. This briefing describes HDL assembly concepts,
elements and build process.

Briefing_10: HDL Primitives. This briefing describes HDL primitives concepts
and elements.

Briefing 11: Introduction to OpenCPI Platform_Development. This briefing
discusses platform development concepts and describes how to enable new
systems, development hosts, GPP platforms and FPGA platforms.

Briefing 12: Device Support for FPGA Platforms. This briefing describes
device support concepts and development and provides information about
device, subdevice and proxy device workers and how to create and use them.

Briefing 13: Advanced OpenCPI Platform Development. This briefing reviews
OpenCPI FPGA platform development concepts and elements and provides a
case study of an OpenCPI FPGA platform implementation.

The Template directory contains the template to be used to create
new briefings.
